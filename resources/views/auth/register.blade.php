@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="contact-from mt-30">
                <div class="section-title">
                    <h5>
                        {{ __('Register') }}
                    </h5>
                </div>

                <div class="main-form">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group row">
                            <div class="col-md-12">
                                <div class="singel-form form-group has-error has-danger">
                                    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" placeholder="Name" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                                    @error('name')
                                    <div class="help-block with-errors">
                                        <ul class="list-unstyled">
                                            <li>
                                                <strong>{{ $message }}</strong>
                                            </li>
                                        </ul>
                                    </div>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-12">
                                <div class="singel-form form-group has-error has-danger">
                                    <input id="email" placeholder="Email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
                                    @error('email')
                                    <div class="help-block with-errors">
                                        <ul class="list-unstyled">
                                            <li>
                                                <strong>{{ $message }}</strong>
                                            </li>
                                        </ul>
                                    </div>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-12">
                                <div class="singel-form form-group has-error has-danger">
                                    <input id="password" placeholder="Password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                                    @error('password')
                                    <div class="help-block with-errors">
                                        <ul class="list-unstyled">
                                            <li>
                                                <strong>{{ $message }}</strong>
                                            </li>
                                        </ul>
                                    </div>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-12">
                                <div class="singel-form form-group has-error has-danger">
                                    <input id="password-confirm" placeholder="Password Confirmation" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 mt-3">
                                <button type="submit" class="main-btn ">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
