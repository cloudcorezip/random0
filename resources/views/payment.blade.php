<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
    <!--====== Required meta tags ======-->
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!--====== Favicon Icon ======-->
    <link rel="shortcut icon" href="{{asset('images/favicon.png')}}" type="image/png">

    <!--====== Slick css ======-->
    <link rel="stylesheet" href="{{asset('css/slick.css')}}">

    <!--====== Animate css ======-->
    <link rel="stylesheet" href="{{asset('css/animate.css')}}">

    <!--====== Nice Number css ======-->
    <link rel="stylesheet" href="{{asset('css/jquery.nice-number.min.css')}}">

    <!--====== Magnific Popup css ======-->
    <link rel="stylesheet" href="{{asset('css/magnific-popup.css')}}">

    <!--====== Bootstrap css ======-->
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">

    <!--====== Fontawesome css ======-->
    <link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}">

    <!--====== Default css ======-->
    <link rel="stylesheet" href="{{asset('css/default.css')}}">

    <!--====== Style css ======-->
    <link rel="stylesheet" href="{{asset('css/style.css')}}">

    <!--====== Responsive css ======-->
    <link rel="stylesheet" href="{{asset('css/responsive.css')}}">

@yield('css')

<!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

</head>
<body>
<div class="preloader">
    <div class="loader rubix-cube">
        <div class="layer layer-1"></div>
        <div class="layer layer-2"></div>
        <div class="layer layer-3 color-1"></div>
        <div class="layer layer-4"></div>
        <div class="layer layer-5"></div>
        <div class="layer layer-6"></div>
        <div class="layer layer-7"></div>
        <div class="layer layer-8"></div>
    </div>
</div>

<div id="app">
    @include('layouts.navigation')

    <section id="courses-part" class="pt-60 pb-120 gray-bg">
        <div class="container">
            <div class="card">
                <div class="card-body">
                    <h2 class="card-title text-center">Payment & Order</h2>
                    @include('message_info')
                    @if(is_null($invoice))
                        @if($userSubscription !== null)
                            <h6 class="card-subtitle my-3 text-muted">You have an active subscription until <strong>{{$userSubscription->expired_at}}</strong></h6>
                        @else
                            <form action="{{route('payment.invoice')}}" method="POST">
                                @csrf
                                <div class="form-group">
                                    <label for="subscription" class="mt-3">Choose Subscription</label>
                                    <select name="subscription" class="form-control">
                                        @foreach($subscriptions as $subscription)
                                            <option value="{{$subscription->id}}">{{$subscription->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                @if(count($paymentMethods) != 0)
                                    <div class="form-group">
                                        <label for="payment_method" class="mt-3">Payment Method</label>
                                        <select name="payment_method" class="form-control">
                                            @foreach($paymentMethods as $paymentMethod)
                                                <option value="{{$paymentMethod->id}}">{{$paymentMethod->name}} - {{$paymentMethod->account_number}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                @endif
                                <button type="submit" class="btn btn-primary mt-3">Subscribe</button>
                            </form>
                        @endif
                    @else
                        @if(is_null($invoice->payment_slip))
                            <h6 class="card-subtitle mb-2 text-muted">You have unpaid payment with invoice number <strong>{{$invoice->invoice_number}}</strong></h6>
                            <h6 class="card-subtitle mb-2 text-muted">Total :  <strong>{{$invoice->price}}</strong></h6>
                            <form action="{{route('payment.slip')}}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="invoice_id" value="{{$invoice->id}}">
                                <div class="form-group">
                                    <label for="image">Payment Slip </label>
                                    <div class="input-group">
                                        <div class="custom-file">
                                            <input type="file" name="image" class="custom-file-input" id="exampleInputFile">
                                            <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                                        </div>
                                        <div class="input-group-append">
                                            <span class="input-group-text">Upload</span>
                                        </div>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary mt-3">Submit</button>
                            </form>
                        @else
                            <h6 class="card-subtitle mb-2 text-muted">Please wait admin verify your payment</h6>
                        @endif
                    @endif
                </div>
            </div>
        </div>
    </section>
    @include('layouts.footer')
    <a href="#" class="back-to-top"><i class="fa fa-angle-up"></i></a>
</div>
</body>
<script src="{{asset('js/vendor/modernizr-3.6.0.min.js')}}"></script>
<script src="{{asset('js/vendor/jquery-1.12.4.min.js')}}"></script>

<!-- Bootstrap 4 -->
<script src="{{asset('plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

<!--====== Slick js ======-->
<script src="{{asset('js/slick.min.js')}}"></script>

<!--====== Magnific Popup js ======-->
<script src="{{asset('js/jquery.magnific-popup.min.js')}}"></script>

<!--====== Counter Up js ======-->
<script src="{{asset('js/waypoints.min.js')}}"></script>
<script src="{{asset('js/jquery.counterup.min.js')}}"></script>

<!--====== Nice Number js ======-->
<script src="{{asset('js/jquery.nice-number.min.js')}}"></script>

<!--====== Count Down js ======-->
<script src="{{asset('js/jquery.countdown.min.js')}}"></script>

<!--====== Validator js ======-->
<script src="{{asset('js/validator.min.js')}}"></script>

<!--====== Ajax Contact js ======-->
<script src="{{asset('js/ajax-contact.js')}}"></script>

<!--====== Main js ======-->
<script src="{{asset('js/main.js')}}"></script>

<!--====== Map js ======-->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDC3Ip9iVC0nIxC6V14CKLQ1HZNF_65qEQ"></script>
<script src="{{asset('js/map-script.js')}}"></script>

@yield('js')
</html>
