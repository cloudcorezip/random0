<aside class="main-sidebar sidebar-light-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{route('admin.dashboard')}}" class="brand-link">
        <img src="{{asset('logo/logo1.png')}}" alt="Pelatihku Logo" class="brand-image img-circle" style="opacity: .8">
        <span class="brand-text font-weight-light">Pelatihku</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                     with font-awesome or any other icon font library -->
                <li class="nav-header">Master</li>
                <li class="nav-item">
                    <a href="{{route('category.index')}}" class="nav-link">
                        <p>
                            Category
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('course.article.index')}}" class="nav-link">
                        <p>
                            Articles
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('course.chapter.index')}}" class="nav-link">
                        <p>
                            Courses
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('payment_method.index')}}" class="nav-link">
                        <p>
                            Payment Methods
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('subscription.index')}}" class="nav-link">
                        <p>
                            Subscriptions
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('user.index')}}" class="nav-link">
                        <p>
                            Users
                        </p>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
