@extends('layouts.app')

@section('css')

@endsection

@section('content')
    <section id="page-banner" class="pt-105 pb-110 bg_cover" data-overlay="8">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="page-banner-cont">
                        <h2>{{$chapter->title}}</h2>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="courses-single" class="pt-90 gray-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="corses-singel-left mt-30">
                        <div class="course-terms">
                            <ul>
                                <li class="float-left">
                                    <a class="mb-4" href="{{route('course.show', $course->id)}}">Back To Course</a>
                                </li>
                                @if($next_course !== null)
                                    <li class="float-right">
                                        <a class="mb-4" href="{{route('chapter.show', $next_course->id)}}">Next Course</a>
                                    </li>
                                @endif
                            </ul>
                        </div>
                        <div>
                            <div class="embed-responsive embed-responsive-16by9">
                                <iframe class="embed-responsive-item" width="1920" height="1080" allowfullscreen src="{{asset($chapter->url)}}"></iframe>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
@endsection
