@extends('layouts.app')

@section('css')

@endsection

@section('content')
    <section id="page-banner" class="pt-105 pb-110 bg_cover" data-overlay="8" >
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="page-banner-cont">
                        <h2>{{$article->title}}</h2>
                    </div>
                </div>
            </div> <!-- row -->
        </div> <!-- container -->
    </section>

    <section id="courses-single" class="pt-90 gray-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="corses-singel-left mt-30">
                        <div class="course-terms">
                            <ul>
                                <li class="float-left">
                                    <a class="mb-4" href="{{route('course.show', $course->id)}}">Back To Course</a>
                                </li>
                            </ul>
                        </div>
                        <div class="corses-tab mt-30">
                            <div class="overview-description">
                                <div class="singel-description pt-40">
                                    <p>{!! $article->content !!}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
