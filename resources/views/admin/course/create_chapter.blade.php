@extends('layouts.admin.layout')

@section('content')
    <div class="container">
        <div class="space">
            @if(count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                    @endif
                    <form action="{{ route('course.store.chapter') }}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="name">Course Name : </label>
                            <input type="text" name="name" class="form-control" placeholder="ex. Technology" value="" required>
                        </div>
                        <div class="form-group">
                            <label for="description">Course Description : </label>
                            <textarea name="description" class="form-control" placeholder="Lorem ipsum" required></textarea>
                        </div>
                        <div class="form-group">
                            <label for="image">Course Banner : </label>
                            <div class="input-group">
                                <div class="custom-file">
                                    <input type="file" name="image" class="custom-file-input" id="exampleInputFile">
                                    <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                                </div>
                                <div class="input-group-append">
                                    <span class="input-group-text">Upload</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group ">
                            <label for="category">Category</label>
                            <select name="category_id" class="custom-select">
                                @foreach($categories as $category)
                                    <option value="{{$category->id}}">{{$category->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <h3>Chapters</h3>
                        <div class="field-wrapper">
                            <div class="form-group">
                                <label for="name">Chapter Title : </label>
                                <input type="text" name="title[]" class="form-control" placeholder="Awesome Chapter" value="" required>
                            </div>
                            <div class="form-group">
                                <label for="name">Chapter URL : </label>
                                <input type="text" name="url[]" class="form-control" placeholder="https://www.youtube.com/embed/asd123" value="" required>
                            </div>
                            <div class="form-group">
                                <div class="custom-control custom-checkbox">
                                    <input class="custom-control-input" type="checkbox" id="customCheckbox" name="premium[]" value="0">
                                    <label for="customCheckbox" class="custom-control-label">Is Premium</label>
                                </div>
                            </div>
                            <a href="javascript:void(0);" class="btn btn-primary add_button mb-3" title="Add field">Add More Chapter</a>
                        </div>

                        <button type="submit" class="btn btn-primary my-3">Submit</button>
                    </form>
                </div>
        </div>
@endsection

@section('scripts')
<script>
    $(document).ready(function(){
        var x = 1;
        var addButton = $('.add_button');
        var wrapper = $('.field-wrapper');

        $(addButton).click(function(){
            const fieldHTML = '<div>' +
                            `
                            <div class="form-group">
                                <label for="name">Chapter Title : </label>
                                <input type="text" name="title[]" class="form-control" placeholder="Awesome Chapter" value="" required>
                            </div>
                            <div class="form-group">
                                <label for="name">Chapter URL : </label>
                                <input type="text" name="url[]" class="form-control" placeholder="https://youtu.be/asd123" value="" required>
                            </div>
                            <div class="custom-control custom-checkbox">
                                    <input class="custom-control-input" id="customCheckbox${x}" type="checkbox" name="premium[]" value="${x}">
                                    <label for="customCheckbox${x}" class="custom-control-label">Is Premium</label>
                                </div>
                            <a href="javascript:void(0);" class="remove_button btn btn-danger my-3">Remove</a>
                         </div>`
            $(wrapper).append(fieldHTML);
            x++
        });

        $(wrapper).on('click', '.remove_button', function(e){
            e.preventDefault();
            $(this).parent('div').remove();
            x--;
        });
    });
</script>
@endsection
