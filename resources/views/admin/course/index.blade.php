@extends('layouts.admin.layout')

@section('styles')
    <link rel="stylesheet" href="{{asset('plugins/datatables/jquery.dataTables.min.css')}}">
    <link rel="stylesheet" href="{{asset('plugins/datatables/extensions/Responsive/css/dataTables.responsive.css')}}">
@endsection

@section('content')
    <div class="container">
        <div class="space">
            @include('message_info')
            <h1 class="text-center">List Course</h1>
            <a class="btn btn-primary mr-2 float-right" href="{{route('course.create.chapter')}}" style="margin-bottom: 20px">+ Add New Chapter Course</a>
            <table class="data-table display nowrap" style="width:100%">
                <thead>
                    <tr>
                        <th width="40%" class="text-center align-middle">Title</th>
                        <th width="30%" class="text-center align-middle">Category</th>
                        <th width="30" class="text-center align-middle">Edit</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js')}}"></script>
    <script>
        $(document).ready(function() {
            $('.data-table').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: '{{ route('course.chapter.index') }}',
                columns: [
                    {data: 'title', name: 'title'},
                    {data: 'category', name: 'category'},
                    {data: 'action', name: 'action', orderable: false, searchable: false, className: "text-center"},
                ]
            });
        });
    </script>
@endpush
