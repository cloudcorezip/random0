@extends('layouts.admin.layout')

@section('content')
    <div class="container">
        <div class="space">
            @if(count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                    @endif
                    <form action="{{ route('course.store.article') }}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="name">Course Name : </label>
                            <input type="text" name="name" class="form-control" placeholder="ex. Technology" value="" required>
                        </div>
                        <div class="form-group">
                            <label for="description">Course Description : </label>
                            <textarea name="description" class="form-control" placeholder="Lorem ipsum" required></textarea>
                        </div>
                        <div class="form-group">
                            <label for="image">Course Banner : </label>
                            <div class="input-group">
                                <div class="custom-file">
                                    <input type="file" name="image" class="custom-file-input" id="exampleInputFile">
                                    <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                                </div>
                                <div class="input-group-append">
                                    <span class="input-group-text">Upload</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group ">
                            <label for="category">Category</label>
                            <select name="category_id" class="custom-select">
                                @foreach($categories as $category)
                                    <option value="{{$category->id}}">{{$category->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <h3>Article</h3>
                        <div class="field-wrapper">
                            <div class="form-group">
                                <label for="name">Article Title : </label>
                                <input type="text" name="article_title" class="form-control" placeholder="ex. Very Interesting Article Title" value="" required>
                            </div>
                            <div class="form-group">
                                <label for="name">Content : </label>
                                <textarea name="article_content" class="my-editor form-control" id="my-editor" cols="30" rows="10"></textarea>
                            </div>
                            <div class="form-group">
                                <div class="custom-control custom-checkbox">
                                    <input class="custom-control-input" type="checkbox" id="customCheckbox" name="premium">
                                    <label for="customCheckbox" class="custom-control-label">Is Premium</label>
                                </div>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-primary my-3">Submit</button>
                    </form>
                </div>
        </div>
@endsection

@section('scripts')
<script src="https://cdn.ckeditor.com/4.6.2/standard/ckeditor.js"></script>
<script>
    var options = {
        filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
        filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token=',
        filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
        filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token='
    };
</script>
<script>
    CKEDITOR.replace('my-editor', options);
</script>
@endsection
