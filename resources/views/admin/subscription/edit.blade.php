@extends('layouts.admin.layout')

@section('content')
    <div class="container">
        <div class="space">
            @if(count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                    @endif
                    <form action="{{ route('subscription.update', $subscription->id) }}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        @method('put')
                        <div class="form-group">
                            <label for="name">Subscription Name : </label>
                            <input type="text" name="name" class="form-control" placeholder="ex. Monthly" value="{{$subscription->name}}" required>
                        </div>
                        <div class="form-group">
                            <label for="name">Subscription Type : </label>
                            <input type="text" name="type" class="form-control" placeholder="ex. Daily" value="{{$subscription->type}}" required>
                        </div>
                        <div class="form-group">
                            <label for="name">Subscription Price : </label>
                            <input type="number" name="price" class="form-control" placeholder="ex. 10000" value="{{$subscription->price}}" required>
                        </div>
                        <div class="form-group">
                            <label for="name">Subscription Price Percentage : </label>
                            <input type="number" name="price_percentage" class="form-control" placeholder="ex. 100" value="{{$subscription->price_percentage}}" required>
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
        </div>
@endsection
