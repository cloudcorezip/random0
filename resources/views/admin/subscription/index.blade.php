@extends('layouts.admin.layout')

@section('content')
    <div class="container">
        <div class="space">
            @include('message_info')
            <h1 class="text-center">List Subscription</h1>
            <a class="btn btn-primary float-right" href="{{route('payment_method.create')}}" style="margin-bottom: 20px">+ Add New Subscription Type</a>
            <table class="table table-bordered table-striped">
                <tr>
                    <th width="40%" class="text-center align-middle">Name</th>
                    <th width="30%" class="text-center align-middle">Status</th>
                    <th width="30" class="text-center align-middle">Edit</th>
                    <th width="30" class="text-center align-middle">Deactivate</th>
                </tr>
                @if(count($subscriptions) <= 0)
                    <tr>
                        <td colspan="7" class="text-center"><strong> No Subscription Type yet</strong></td>
                    </tr>
                @else
                    @foreach ($subscriptions as $subscription)
                        <tr>
                            <td class="text-center align-middle">{{ $subscription->name }}<br></td>
                            <td class="text-center align-middle">@if($subscription->status) Activated @else Deactivated @endif</td>
                            <td class="text-center align-middle">
                                <a class="btn btn-primary text-center" href="{{route('subscription.edit', $subscription->id)}}">Edit</a>
                            </td>
                            <td class="text-center align-middle">
                                <form class="d-inline" action="{{route('subscription.changeStatus', $subscription->id)}}" method="post">
                                    @csrf
                                    <button class="btn @if($subscription->status) btn-danger @else btn-success @endif" type="submit" onclick="return confirm('@if($subscription->status) Deactivate @else Activate @endif subscription?')">
                                        @if($subscription->status) Deactivate @else Activate @endif
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                @endif
            </table>
            {{$subscriptions->links()}}
        </div>
    </div>
@endsection
