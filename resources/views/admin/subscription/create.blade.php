@extends('layouts.admin.layout')

@section('content')
    <div class="container">
        <div class="space">
            @if(count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                    @endif
                    <form action="{{ route('subscription.store') }}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="name">Subscription Name : </label>
                            <input type="text" name="name" class="form-control" placeholder="ex. Monthly" value="" required>
                        </div>
                        <div class="form-group">
                            <label for="name">Subscription Type : </label>
                            <input type="text" name="type" class="form-control" placeholder="ex. Daily" value="" required>
                        </div>
                        <div class="form-group">
                            <label for="name">Subscription Price : </label>
                            <input type="number" name="price" class="form-control" placeholder="ex. 10000" value="" required>
                        </div>
                        <div class="form-group">
                            <label for="name">Subscription Price Percentage : </label>
                            <input type="number" name="price_percentage" class="form-control" placeholder="ex. 100" value="" required>
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
        </div>
@endsection
