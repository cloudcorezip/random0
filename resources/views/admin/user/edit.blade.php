@extends('layouts.admin.layout')

@section('content')
    <div class="container">
        <div class="space">
            @if(count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                    @endif
                    <form action="{{ route('payment_method.update', $paymentMethod->id) }}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        @method('put')
                        <div class="form-group">
                            <label for="name">Payment Method Name : </label>
                            <input type="text" name="name" class="form-control" placeholder="ex. Gopay" value="{{$paymentMethod->name}}" required>
                        </div>
                        <div class="form-group">
                            <label for="name">Account Number : </label>
                            <input type="text" name="account_number" class="form-control" placeholder="3894023" value="{{$paymentMethod->account_number}}" required>
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
        </div>
@endsection
