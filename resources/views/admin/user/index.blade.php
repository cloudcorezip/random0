@extends('layouts.admin.layout')

@section('styles')
    <link rel="stylesheet" href="{{asset('plugins/datatables/jquery.dataTables.min.css')}}">
    <link rel="stylesheet" href="{{asset('plugins/datatables/extensions/Responsive/css/dataTables.responsive.css')}}">
@endsection

@section('content')
    <div class="container">
        <div class="space">
            @include('message_info')
            <h1 class="text-center">List Users</h1>
            <div class="mt-3">
                <table class="data-table display nowrap" style="width:100%">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Subscription</th>
                        <th>Invoice Number</th>
                        <th>Payment Slip</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js')}}"></script>
    <script>
        $(document).ready(function() {
            $('.data-table').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: '{{ route('user.index') }}',
                columns: [
                    {data: 'name', name: 'name'},
                    {data: 'email', name: 'email'},
                    {data: 'subscription', name: 'subscription', orderable: false, searchable: false},
                    {data: 'invoice_number', name: 'invoice_number', orderable: false, searchable: false},
                    {data: 'image', name: 'image', orderable: false, searchable: false},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });
        });
    </script>
@endpush
