@extends('layouts.admin.layout')

@section('content')
    <div class="container">
        <div class="space">
            @include('message_info')
            <h1 class="text-center">List Payment Methods</h1>
            <a class="btn btn-primary float-right" href="{{route('payment_method.create')}}" style="margin-bottom: 20px">+ Add New Payment Method</a>
            <table class="table table-bordered table-striped">
                <tr>
                    <th width="40%" class="text-center align-middle">Name</th>
                    <th width="40%" class="text-center align-middle">Account Number</th>
                    <th width="30%" class="text-center align-middle">Status</th>
                    <th width="30" class="text-center align-middle">Edit</th>
                    <th width="30" class="text-center align-middle">Deactivate</th>
                </tr>
                @if(count($paymentMethods) <= 0)
                    <tr>
                        <td colspan="7" class="text-center"><strong> No Payment Method yet</strong></td>
                    </tr>
                @else
                    @foreach ($paymentMethods as $paymentMethod)
                        <tr>
                            <td class="text-center align-middle">{{ $paymentMethod->name }}<br></td>
                            <td class="text-center align-middle">{{ $paymentMethod->account_number }}<br></td>
                            <td class="text-center align-middle">@if($paymentMethod->status) Activated @else Deactivated @endif</td>
                            <td class="text-center align-middle">
                                <a class="btn btn-primary text-center" href="{{route('payment_method.edit', $paymentMethod->id)}}">Edit</a>
                            </td>
                            <td class="text-center align-middle">
                                <form class="d-inline" action="{{route('payment_method.changeStatus', $paymentMethod->id)}}" method="post">
                                    @csrf
                                    <button class="btn @if($paymentMethod->status) btn-danger @else btn-success @endif" type="submit" onclick="return confirm('@if($paymentMethod->status) Deactivate @else Activate @endif payment method?')">
                                        @if($paymentMethod->status) Deactivate @else Activate @endif
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                @endif
            </table>
        </div>
    </div>
@endsection
