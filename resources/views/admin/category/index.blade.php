@extends('layouts.admin.layout')

@section('content')
    <div class="container">
        <div class="space">
            @include('message_info')
            <h1 class="text-center">List Category</h1>
            <a class="btn btn-primary float-right" href="{{route('category.create')}}" style="margin-bottom: 20px">+ Add New Category</a>
            <table class="table table-bordered table-striped">
                <tr>
                    <th width="40%" class="text-center align-middle">Name</th>
                    <th width="30%" class="text-center align-middle">Status</th>
                    <th width="30" class="text-center align-middle">Edit</th>
                    <th width="30" class="text-center align-middle">Deactivate</th>
                </tr>
                @if(count($categories) <= 0)
                    <tr>
                        <td colspan="7" class="text-center"><strong> No Category yet</strong></td>
                    </tr>
                @else
                    @foreach ($categories as $category)
                        <tr>
                            <td class="text-center align-middle">{{ $category->name }}<br></td>
                            <td class="text-center align-middle">@if($category->status) Activated @else Deactivated @endif</td>
                            <td class="text-center align-middle">
                                <a class="btn btn-primary text-center" href="{{route('category.edit', $category->id)}}">Edit</a>
                            </td>
                            <td class="text-center align-middle">
                                <form class="d-inline" action="{{route('category.changeStatus', $category->id)}}" method="post">
                                    @csrf
                                    <button class="btn @if($category->status) btn-danger @else btn-success @endif" type="submit" onclick="return confirm('@if($category->status) Deactivate @else Activate @endif category?')">
                                        @if($category->status) Deactivate @else Activate @endif
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                @endif
            </table>
            {{$categories->links()}}
        </div>
    </div>
@endsection
