@extends('layouts.app')

@section('css')

@endsection

@section('content')
    <section id="page-banner" class="pt-105 pb-110 bg_cover" data-overlay="8">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="page-banner-cont">
                        <h2>{{$course->title}}</h2>
                    </div>
                </div>
            </div> <!-- row -->
        </div> <!-- container -->
    </section>

    <section id="courses-single" class="pt-90 gray-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="corses-singel-left mt-30">
                        <div class="course-terms">
                            <ul>
                                <li>
                                    <div class="course-category">
                                        <span>Category</span>
                                        <h6>{{$course->category->name}} </h6>
                                        @if($course->is_article && optional($course->article)->is_premium)
                                            <h6 class="mt-2"><strong>PREMIUM COURSE</strong> </h6>
                                        @endif
                                    </div>
                                </li>
                            </ul>
                        </div>

                        <div class="corses-singel-image pt-50">
                            <img src="{{asset($course->banner)}}" alt="Courses">
                        </div>

                        <div class="corses-tab mt-30">
                            <ul class="nav nav-justified" id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="active" id="overview-tab" data-toggle="tab" href="#overview" role="tab" aria-controls="overview" aria-selected="true">Overview</a>
                                </li>
                                @if(!$course->is_article)
                                    <li class="nav-item">
                                        <a id="curriculam-tab" data-toggle="tab" href="#curriculam" role="tab" aria-controls="curriculam" aria-selected="false">Curriculum</a>
                                    </li>
                                @endif
                            </ul>

                            <div class="tab-content" id="myTabContent">
                                <div class="tab-pane fade show active" id="overview" role="tabpanel" aria-labelledby="overview-tab">
                                    <div class="overview-description">
                                        <div class="singel-description pt-40">
                                            <h6>Course Summary</h6>
                                            <p>{{$course->description}}</p>
                                        </div>
                                        @if($course->is_article)
                                            <a class="mt-3" href="{{route('article.show', $course->article->id)}}">Read Course</a>
                                        @endif
                                    </div>
                                </div>
                                @if(!$course->is_article)
                                    <div class="tab-pane fade" id="curriculam" role="tabpanel" aria-labelledby="curriculam-tab">
                                    <div class="curriculam-cont">
                                        <div class="accordion" id="accordionExample">
                                            @foreach($course->chapter as $key=>$chapter)
                                                <div class="card">
                                                <div class="card-header" id="heading{{$key}}">
                                                    <a href="#" data-toggle="collapse" data-target="#collapse{{$key}}" aria-expanded="true" aria-controls="collapse{{$key}}">
                                                        <ul>
                                                            <li><i class="fa fa-file-o"></i></li>
                                                            <li><span class="lecture">{{$chapter->title}} @if($chapter->is_premium) - <strong>PREMIUM</strong> @endif</span></li>
                                                            <li><span class="head"></span></li>
                                                        </ul>
                                                    </a>
                                                </div>

                                                <div id="collapse{{$key}}" class="collapse" aria-labelledby="heading{{$key}}" data-parent="#accordionExample">
                                                    <div class="card-body">
                                                        <a href="{{route('chapter.show', $chapter->id)}}">Watch Chapter</a>
                                                    </div>
                                                </div>
                                            </div>
                                            @endforeach
                                        </div>
                                    </div> <!-- curriculam cont -->
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
@endsection
