@extends('layouts.app')

@section('content')
    <div class="container">
        <h5>Tentang kami</h5>
        <p>Pelatihku merupakan platform edukasi dibawah naungan Universitas Negeri Yogyakarta sebagai platform pembelajaran olahraga secara online yang memberikan kemudahan akses bagi masyarakat dalam meningkatkan kemahiran dan kompetensinya sesuai dengan kebutuhan olahraga pada saat ini maupun mendatang.
        Dengan bekerjasama dengan atlet dan lembaga pelatihan, pelatihku memberikan program pelatihan dan pembelajaran yang terbaik guna memberikan manfaat yang besar bagi masyarakat. Pelatihku senantiasa memfasilitasi pengguna platform pelatihku agar dapat mengikuti pembelajaran serta pelatihan sesuai dengan minat dan bakat sehingga pelatihku dapat membantu meningkatkan kualitas olahraga di Indonesia.</p>
        <br/>
        <h6>Kenapa harus pelatihku</h6>
        <ul style="list-style-type: circle">
            <li>Terdapat berbagai video pembelajaran olahraga yang dapat diakses kapanpun dan dimanapun dengan mudah menggunakan akses internet.</li>
            <li>Bekerjasama dengan atlet dan pelatih serta salah satu lembaga pendidikan terbaik di Indonesia.</li>
            <li>Menyediakan video pembelajaran yang mudah dipahami.</li>
        </ul>
        <br/>
        <h5>Bantuan</h5>
        <h6>Apa itu pelatihku?</h6>
        <p>Pelatihku merupakan platform pembelajaran digital berfokus pada olahraga edukasi untuk masyarakat Indonesia. Pelatihku memberikan video pembelajaran dengan bekerjasama dengan atlet dan pelatih.</p>
        <br/>
        <h6>Mengapa pelatihku?</h6>
        <ul style="list-style-type: circle">
            <li>Platform digital yang terfokus pada olahraga edukasi</li>
            <li>Bermitra dengan salah satu lembaga pendidikan terbaik di Indonesia</li>
            <li>Simple, mudah, dan praktis.</li>
        </ul>
        <br/>
        <h6>Apa keuntungan memilih pelatihku?</h6>
        <p>Pengguna dapat mengakses video pembelajaran secara online maupun offline. Video dilengkapi dengan deskripsi olahraga sehingga pengguna selain melihat juga dapat membaca yang bertujuan menambah pengetahuan pengguna.</p>
        <br/>
        <h6>Bagaimana cara membuat akun pelatihku?</h6>
        <ul style="list-style-type: circle">
            <li>Klik daftar</li>
            <li>Isi data diri lengkap</li>
            <li>Centang kotak untuk menyetujui ketentuan penggunaan dan kebijakan privasi kami</li>
            <li>Cek email dan verifikasi akun</li>
            <li>Silahkan masuk dengan email dan password yang telah diverifikasi</li>
        </ul>
        <br/>
        <h6>Bagaimana cara berlangganan secara premium?</h6>
        <ul style="list-style-type: circle">
            <li>Pilih berlangganan premium</li>
            <li>Klik berlangganan premium</li>
            <li>Pilih metode pembayaran</li>
            <li>Klik bayar</li>
            <li>Cek email untuk melihat status berlangganan</li>
            <li>Apabila pembayaran telah berhasil maka pengguna bisa mengakses video premium</li>
        </ul>
        <br/>
        <h6>Metode pembayaran apa saja yang bisa digunakan pelatihku?</h6>
        <ul style="list-style-type: circle">
            <li>Shopee</li>
            <li>OVO</li>
            <li>Dana</li>
            <li>Bank</li>
        </ul>
        <br/>
        <h5>Syarat dan ketentuan</h5>
        <h6>Pendaftaran</h6>
        <ul style="list-style-type: circle">
            <li>Untuk Registrasi menjadi user anda harus mengisi semua kelengkapan biodata registrasi tanpa terkecuali dan menyertakan identitas asli Anda.</li>
            <li>Kontak yang anda cantumkan adalah kontak aktif, sehingga Pelatihku dapat menghubungi sewaktu-waktu apabila diperlukan.</li>
            <li>Pelatihku berhak menolak aplikasi pendaftaran Anda sebagai User dengan alasan yang jelas.</li>
        </ul>
        <br/>
        <h6>Akun Pengguna</h6>
        <ul style="list-style-type: circle">
            <li>Anda bertanggung jawab untuk menjaga kerahasiaan dan keamanan akun dan password Anda.</li>
            <li>Anda menyetujui untuk bertanggung jawab atas semua yang terjadi terkait penggunaan akun pribadi dan password anda.</li>
            <li>Anda menyetujui untuk melaporkan pada Pelatihku tentang setiap penyalahgunaan akun atau pelanggaran keamanan lainnya yang berkaitan dengan akun Anda secepat mungkin.</li>
            <li>Pelatihku tidak bertanggung jawab atas kerugian atau kerusakan yang timbul bila anda tidak mematuhi persyaratan-persyaratan ini.</li>
            <li>Pelatihku berhak untuk menolak layanan atau menghentikan akun anda dengan alasan yang jelas.</li>
        </ul>
        <br/>
        <h6>Profil pengguna</h6>
        <ul style="list-style-type: circle">
            <li>Anda setuju untuk mengisi semua informasi pribadi dengan data yang benar dan akurat.</li>
        </ul>
        <br/>
        <h6>Materi dan Informasi</h6>
        <ul style="list-style-type: circle">
            <li>Semua materi dan informasi disediakan oleh Pelatihku</li>
            <li>Pelatihku tidak bertanggung jawab atas segala kerugian yang disebabkan oleh materi dan informasi diluar Pelatihku</li>
        </ul>
        <br/>
        <h6>Pembayaran</h6>
        <ul style="list-style-type: circle">
            <li>Pengguna setuju untuk membayar segala jenis materi dan informasi berbayar yang ditetapkan oleh Pelatihku.</li>
            <li>Apabila pengguna tidak membayar dalam waktu yang telah disepakati, Pelatihku berhak membatalkan akses anda terhadap materi dan informasi tertentu.</li>
            <li>Pelatihku tidak menjamin pengembalian pembayaran apabila terjadi pelanggaran terhadap syarat dan ketentuan.</li>
        </ul>
        <br/>
        <h6>Penggunaan Informasi</h6>
        <ul style="list-style-type: circle">
            <li>Penggunaan Informasi akun untuk kepentingan Pelatihku diperbolehkan dengan persetujuan pihak Pelatihku tanpa melalui perantara.</li>
            <li>Segala hal yang berkaitan dengan Informasi, sepenuhnya adalah hak Pelatihku.</li>
        </ul>
        <br/>
        <h5>Kebijakan Privasi</h5>
        <br/>
        <h6>Informasi pribadi</h6>
        <p>Informasi pribadi yang dimaksud dalam kebijakan privasi ini adalah setiap informasi tentang diri anda yang dimasukkan ketika menggunakan platform digital Pelatihku, contohnya ketika anda mendaftarkan diri untuk membuat akun yang termasuk nama, informasi kontak, gender, tanggal lahir, maupun informasi lain yang anda masukkan ketika melakukan akse layanan digital.
        <br/><br/>
        <h6>Penggunaan dan Pernyataan kepada pihak Ketiga</h6>
        <p>Kami dan mitra yang menyediakan pembelajaran olahraga melalui pelatihku dapat menggunakan , membagikan informasi anda yang didapat melalui layanan Pelatihku ( termasuk informasi pribadi ) dengan ketentuan sebagai berikut: Pengguna diwajibkan menjaga kerahasiaan, keamanan dan melindungi informasi dari segala resiko maupun kerugian akibat pengungkapan informasi.</p>
        <br/>
        <p>
            Ayo bermitra dengan Pelatihku!
            Dengan semangat kolaborasi, Pelatihku mengajak masyarakat untuk menjadi mitra dalam menghadirkan kesempatan belajar melalui platform Pelatihku. Hubungi kami : email infinitysport30@gmail.com .
        </p>


    </div> <!-- container -->
@endsection
