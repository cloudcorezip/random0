@extends('layouts.app')

@section('css')
    <link
        rel="stylesheet"
        href="https://unpkg.com/swiper/swiper-bundle.min.css"
    />
<style>
    .swiper {
        width: 100%;
        height: 100%;
    }

    .swiper-slide {
        text-align: center;
        font-size: 18px;
        background: #fff;

        /* Center slide text vertically */
        display: -webkit-box;
        display: -ms-flexbox;
        display: -webkit-flex;
        display: flex;
        -webkit-box-pack: center;
        -ms-flex-pack: center;
        -webkit-justify-content: center;
        justify-content: center;
        -webkit-box-align: center;
        -ms-flex-align: center;
        -webkit-align-items: center;
        align-items: center;
    }

    .swiper-slide img {
        display: block;
        width: 100%;
        height: 100%;
        object-fit: cover;
    }
</style>
@endsection

@section('content')
    <div class="container">
        <h4 class="mb-2">Categories</h4>
        <div class="swiper mySwiper mb-5">
            <div class="swiper-wrapper">
                @foreach($categories as $category)
                    <a class="swiper-slide" href="{{route('dashboard')}}?category={{$category->id}}">
                        <div class="">
                            <div class="card-img"><img style="max-height: 200px" src="{{asset($category->category_image)}}"></div>
                            <div class="card-body">
                                <h5 class="mb-3">{{$category->name}}</h5>
                            </div>
                        </div>
                    </a>
                @endforeach
            </div>
            <div class="swiper-pagination"></div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="courses-top-search">
                    <ul class="nav float-left" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="active" id="courses-grid-tab" data-toggle="tab" href="#courses-grid" role="tab" aria-controls="courses-grid" aria-selected="true"><i class="fa fa-th-large"></i></a>
                        </li>
                        <li class="nav-item">
                            <a id="courses-list-tab" data-toggle="tab" href="#courses-list" role="tab" aria-controls="courses-list" aria-selected="false"><i class="fa fa-th-list"></i></a>
                        </li>
                        <li class="nav-item">Showing {{$courses->count()}} of {{$total}} Results</li>
                    </ul> <!-- nav -->
                </div>
            </div>
        </div> <!-- row -->
        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active" id="courses-grid" role="tabpanel" aria-labelledby="courses-grid-tab">
                <div class="row">
                    @foreach($courses as $course)
                        <div class="col-lg-4 col-md-6">
                        <div class="singel-course mt-30">
                            <div class="cont">
                                <a href="{{route('course.show', $course->id)}}"><h4>{{$course->title}}</h4></a>
                                <img class="mb-3 card-img-top" style="max-height: 200px" src="{{ $course->banner }}" alt="Course Banner">
                                <p>{!! \Illuminate\Support\Str::limit( strip_tags( $course->description ), 50 ) !!}</p>
                                <div class="course-teacher ">
                                    <div class="admin float-left">
                                        <ul>
                                            <li><a href="#"><span>{{$course->name}}</span></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div> <!-- singel course -->
                    </div>
                    @endforeach
                </div> <!-- row -->
            </div>
            <div class="tab-pane fade" id="courses-list" role="tabpanel" aria-labelledby="courses-list-tab">
                <div class="row">
                    @foreach($courses as $course)
                        <div class="col-lg-12">
                            <div class="singel-course mt-30">
                                <div class="row no-gutters">
                                    <div class="col-md-12">
                                        <div class="cont">
                                            <a href="{{route('course.show', $course->id)}}"><h4>{{$course->title}}</h4></a>
                                            <p>{!! \Illuminate\Support\Str::limit( strip_tags( $course->description ), 50 ) !!}</p>
                                            <div class="course-teacher ">
                                                <div class="admin float-left">
                                                    <ul>
                                                        <li><a href="#"><span>{{$course->name}}</span></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div> <!--  row  -->
                            </div> <!-- singel course -->
                        </div>
                    @endforeach
                </div> <!-- row -->
            </div>
        </div> <!-- tab content -->
        <div class="row">
            <div class="col-lg-12">
                <nav class="courses-pagination mt-50">
                    <ul class="pagination justify-content-center">
                        <li class="page-item">
                            @if($courses->onFirstPage())
                                <a href="#" aria-label="Previous">
                                    <i href="#" class="fa fa-angle-left"></i>
                                </a>
                            @else
                                @if($type)
                                    <a href="{{$courses->previousPageUrl()}}&type={{$type}}" aria-label="Previous">
                                        <i class="fa fa-angle-left"></i>
                                    </a>
                                @else
                                    <a href="{{$courses->previousPageUrl()}}" aria-label="Previous">
                                        <i class="fa fa-angle-left"></i>
                                    </a>
                                @endif
                            @endif
                        </li>
                        <li class="page-item">
                            @if($courses->nextPageUrl() != "")
                                @if($type)
                                    <a href="{{$courses->nextPageUrl()}}&type={{$type}}" aria-label="Next">
                                        <i class="fa fa-angle-right"></i>
                                    </a>
                                @else
                                    <a href="{{$courses->nextPageUrl()}}" aria-label="Next">
                                        <i class="fa fa-angle-right"></i>
                                    </a>
                                @endif
                            @else
                                <a href="#" aria-label="Next">
                                    <i class="fa fa-angle-right"></i>
                                </a>
                            @endif
                        </li>
                    </ul>
                </nav>  <!-- courses pagination -->
            </div>
        </div>  <!-- row -->
    </div> <!-- container -->
@endsection

@section('js')
    <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>

    <!-- Initialize Swiper -->
    <script>
        var swiper = new Swiper(".mySwiper", {
            slidesPerView: 3,
            spaceBetween: 30,
            pagination: {
                el: ".swiper-pagination",
                clickable: true,
            },
        });
    </script>
@endsection
