<?php

use App\Http\Controllers\ArticleController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ChapterController;
use App\Http\Controllers\CourseController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\PaymentController;
use App\Http\Controllers\PaymentMethodController;
use App\Http\Controllers\SubscriptionController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use UniSharp\LaravelFilemanager\Lfm;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['verify' => true]);

Route::get('/', [DashboardController::class, 'dashboard'])->name('dashboard');
Route::get('/about', [DashboardController::class, 'about'])->name('about');

Route::get('/course/{course}', [CourseController::class, 'show'])->name('course.show');

Route::middleware('auth')->group(function() {
    Route::get('/chapter/{chapter}', [ChapterController::class, 'show'])->name('chapter.show');
    Route::get('/article/{article}', [ArticleController::class, 'show'])->name('article.show');
    Route::get('/payment', [PaymentController::class, 'index'])->name('payment.index');
    Route::post('/payment', [PaymentController::class, 'createInvoice'])->name('payment.invoice');
    Route::post('/payment/slip', [PaymentController::class, 'uploadPaymentSlip'])->name('payment.slip');
});

Route::middleware('auth')->group(function() {

    Route::prefix('/admin')->middleware('role:admin')->group(function() {
        Route::get('/', [UserController::class, 'adminDashboard'])->name('admin.dashboard');
        Route::prefix('/category')->group(function() {
            Route::get('/', [CategoryController::class, 'index'])->name('category.index');
            Route::get('/create', [CategoryController::class, 'create'])->name('category.create');
            Route::post('/create', [CategoryController::class, 'store'])->name('category.store');
            Route::get('/edit/{category}', [CategoryController::class, 'edit'])->name('category.edit');
            Route::put('/edit/{category}', [CategoryController::class, 'update'])->name('category.update');
            Route::post('/status/{category}', [CategoryController::class, 'changeStatus'])->name('category.changeStatus');
        });

        Route::prefix('/course')->group(function() {
            Route::get('/chapter', [CourseController::class, 'chapter'])->name('course.chapter.index');
            Route::get('/article', [CourseController::class, 'article'])->name('course.article.index');
            Route::get('/create/article', [CourseController::class, 'createArticle'])->name('course.create.article');
            Route::post('/create/article', [CourseController::class, 'storeArticle'])->name('course.store.article');
            Route::get('/create/chapter', [CourseController::class, 'createChapter'])->name('course.create.chapter');
            Route::post('/create/chapter', [CourseController::class, 'storeChapter'])->name('course.store.chapter');
            Route::get('/edit/chapter/{course}', [CourseController::class, 'editChapter'])->name('course.edit.chapter');
            Route::put('/edit/chapter/{course}', [CourseController::class, 'updateChapter'])->name('course.update.chapter');
            Route::get('/edit/article/{course}', [CourseController::class, 'editArticle'])->name('course.edit.article');
            Route::put('/edit/article/{course}', [CourseController::class, 'updateArticle'])->name('course.update.article');
        });

        Route::prefix('/payment-method')->group(function() {
            Route::get('/', [PaymentMethodController::class, 'index'])->name('payment_method.index');
            Route::get('/create', [PaymentMethodController::class, 'create'])->name('payment_method.create');
            Route::post('/create', [PaymentMethodController::class, 'store'])->name('payment_method.store');
            Route::get('/edit/{paymentMethod}', [PaymentMethodController::class, 'edit'])->name('payment_method.edit');
            Route::put('/edit/{paymentMethod}', [PaymentMethodController::class, 'update'])->name('payment_method.update');
            Route::post('/status/{paymentMethod}', [PaymentMethodController::class, 'changeStatus'])->name('payment_method.changeStatus');
        });

        Route::prefix('/user')->group(function(){
            Route::get('/', [UserController::class, 'index'])->name('user.index');
            Route::get('/table', [UserController::class, 'table'])->name('user.table');
            Route::get('/verify/{user}', [UserController::class, 'verify'])->name('user.verify');
        });

        Route::prefix('/subscription')->group(function() {
            Route::get('/', [SubscriptionController::class, 'index'])->name('subscription.index');
            Route::get('/create', [SubscriptionController::class, 'create'])->name('subscription.create');
            Route::post('/create', [SubscriptionController::class, 'store'])->name('subscription.store');
            Route::get('/edit/{subscription}', [SubscriptionController::class, 'edit'])->name('subscription.edit');
            Route::put('/edit/{subscription}', [SubscriptionController::class, 'update'])->name('subscription.update');
            Route::post('/status/{subscription}', [SubscriptionController::class, 'changeStatus'])->name('subscription.changeStatus');
        });
    });
});

Route::group(['prefix' => 'laravel-filemanager', 'middleware' => ['web', 'auth']], function () {
    Lfm::routes();
});
