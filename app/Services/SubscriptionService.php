<?php

namespace App\Services;

use App\Models\Subscription;
use App\Repositories\SubscriptionRepository;
use Illuminate\Http\Request;

class SubscriptionService
{
    private $subscriptionRepository;

    public function __construct(SubscriptionRepository $subscriptionRepository)
    {
        $this->subscriptionRepository = $subscriptionRepository;
    }

    public function updateSubscription(Subscription $subscription, Request $request) {
        $banner = '';
        $data = collect([
            'name' => $request->name,
            'type' => $request->type,
            'price' => $request->type,
            'price_percentage' => $request->price_percentage,
            'banner' => $banner
        ]);

        //upload banner

        $this->subscriptionRepository->updateSubscription($subscription, $data);
    }
}
