<?php

namespace App\Services;

use App\Models\Category;
use App\Repositories\CategoryRepository;
use Carbon\Carbon;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

class CategoryService
{
    private $categoryRepository;

    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    public function getCategories(): LengthAwarePaginator
    {
        return $this->categoryRepository->getCategories();
    }

    public function create($data) {
        $image = $data->file('image');
        $ext = $image->getClientOriginalExtension();
        $imageName = Carbon::now()->format('dmYHis') . '.' . $ext;

        $image->move('storage/image/', $imageName);
        $data->imagePath = 'storage/image/' . $imageName;

        return $this->categoryRepository->create($data);
    }

    public function update(Category $category, $data) {
        $image = $data->file('image');
        if(is_null($image)){
            $data->imagePath = $category->category_image;
        }else {
            $ext = $image->getClientOriginalExtension();
            $imageName = Carbon::now()->format('dmYHis') . '.' . $ext;

            $image->move('storage/image/', $imageName);
            $data->imagePath = 'storage/image/' . $imageName;
        }

        return $this->categoryRepository->update($category, $data);
    }

    public function changeStatus(Category $category): bool
    {
        return $this->categoryRepository->changeStatus($category);
    }
}
