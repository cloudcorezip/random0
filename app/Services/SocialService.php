<?php

namespace App\Services;

use App\Repositories\SocialRepository;

class SocialService
{
    private $socialRepository;
    public function __construct(SocialRepository $socialRepository)
    {
        $this->socialRepository = $socialRepository;
    }

    public function updateSocial(){
        $this->socialRepository->updateSocial();
    }
}
