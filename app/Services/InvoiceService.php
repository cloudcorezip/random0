<?php

namespace App\Services;

use App\Repositories\InvoiceRepository;

class InvoiceService
{
    private $invoiceRepository;

    public function __construct(InvoiceRepository $invoiceRepository)
    {
        $this->invoiceRepository = $invoiceRepository;
    }

    function verifyPayment($userId): void
    {
        $this->invoiceRepository->setUserInvoiceStatusPaid($userId);
    }
}
