<?php

namespace App\Services;

use App\Repositories\CourseRepository;
use Illuminate\Pagination\CursorPaginator;
use Illuminate\Pagination\LengthAwarePaginator;

class CourseService
{
    private $courseRepository;

    public function __construct(CourseRepository $courseRepository)
    {
        $this->courseRepository = $courseRepository;
    }

    public function fetchAll() : LengthAwarePaginator {
        return $this->courseRepository->getAllCourse();
    }

    public function fetchByType($type) : LengthAwarePaginator {
        return $this->courseRepository->getCourseByType($type);
    }

    public function courseCount(): int
    {
        return $this->courseRepository->courseCount();
    }

    public function courseCountByType($type): int
    {
        return $this->courseRepository->courseCountByType($type);
    }

    public function create($data) {
        return $this->courseRepository->create($data);
    }
}
