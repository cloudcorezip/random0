<?php

namespace App\Repositories;

use App\Models\Course;
use Illuminate\Pagination\CursorPaginator;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;

class CourseRepository
{
    public function getAllCourse() : LengthAwarePaginator {
        return DB::table('courses')
            ->join('categories', 'categories.id', '=', 'courses.category_id')
            ->select('courses.*', 'categories.name')
            ->orderBy('courses.id')->paginate(6);
    }

    public function getCourseByType($type) : LengthAwarePaginator {
        if($type === 'article') {
            return DB::table('courses')->where('is_article', true)->orderBy('id')->paginate(6);
        }

        return DB::table('courses')->where('is_article', false)->orderBy('id')->paginate(6);
    }

    public function courseCount(): int
    {
        return DB::table('courses')->count();
    }

    public function courseCountByType($type): int
    {
        if($type === 'article') {
            return DB::table('courses')->where('is_article', true)->count();
        }

        return DB::table('courses')->where('is_article', false)->count();
    }

    public function create($data) {
        return Course::create([
            'title' => $data->title,
            'description' => $data->description,
            'is_article' => $data->is_article,
            'category_id' => $data->category_id
        ]);
    }
}
