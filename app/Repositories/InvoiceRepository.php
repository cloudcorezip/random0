<?php

namespace App\Repositories;
use App\Models\Invoice;
use App\Models\UserSubscription;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class InvoiceRepository
{
    function setUserInvoiceStatusPaid($userId) {
        $invoice = Invoice::with('subscription')->where('user_id', $userId)->latest('invoices.created_at')->first();

        $expiredAt = Carbon::now();
        $type = optional(optional($invoice)->subscription)->type;
        if(is_null($type)){
            return null;
        }

        switch($type) {
            case 'daily':
                $expiredAt->addDays(1);
                break;
            case 'monthly':
                $expiredAt->addDays(30);
                break;
            case 'anually':
                $expiredAt->addDays(365);
                break;
            default:
                return null;
        }
        DB::table('invoices')->where([
            'user_id' => $userId,
            'is_paid' => false,
        ])->latest()->update([
            'is_paid' => true
        ]);


        UserSubscription::create([
            'user_id' => $userId,
            'subscription_id' => $invoice->subscription_id,
            'status' => 1,
            'expired_at' => $expiredAt
        ]);
    }
}
