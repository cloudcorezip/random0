<?php

namespace App\Repositories;

use App\Models\Subscription;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class SubscriptionRepository
{
    public function updateSubscription(Subscription $subscription, Collection $collection){
        $data = $collection->first();
        $subscription->update([
            'name' => $data->name,
            'type' => $data->type,
            'price' => $data->price,
            'banner' => $data->banner,
            'price_percentage' => $data->price_percentage
        ]);
    }
}
