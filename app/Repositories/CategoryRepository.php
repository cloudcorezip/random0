<?php

namespace App\Repositories;

use App\Models\Category;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;

class CategoryRepository
{
    public function getCategories(): LengthAwarePaginator
    {
        return DB::table('categories')->paginate(5);
    }

    public function create($data) {
        return Category::create([
            'name' => $data->name,
            'category_image' => $data->imagePath,
            'status' => true
        ]);
    }

    public function update(Category $category, $data) {
        return DB::table('categories')->where('id', $category->id)->update([
            'name' => $data->name,
            'category_image' => $data->imagePath
        ]);
    }

    public function changeStatus(Category $category): bool
    {
        if($category->status == false) {
            $category->status = true;
            return $category->save();
        }

        $category->status = false;
        return $category->save();
    }
}
