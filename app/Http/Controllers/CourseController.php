<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Category;
use App\Models\Chapter;
use App\Models\Course;
use App\Services\CourseService;
use Carbon\Carbon;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class CourseController extends Controller
{
    private $courseService;

    public function __construct(CourseService $courseService)
    {
        $this->courseService = $courseService;
    }

    public function show(Course $course) {
        return view('course', [
            'course' => Course::with('article', 'chapter')->where('id', $course->id)->first()
        ]);
    }

    public function createCourse(Request $request): RedirectResponse
    {
        $this->courseService->create($request);

        return redirect()->back()->with('success', 'success creating course');
    }

    public function chapter(Request $request) {
        $data = Course::with('category')->where('is_article', false)->get();
        if ($request->ajax()) {
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('title', function($row) {
                    return $row->title;
                })
                ->addColumn('category', function($row) {
                    return $row->category->name;
                })
                ->addColumn('action', function($row) {
                    $urlEdit = route('course.edit.chapter', $row->id);
                    return '<a href="' . $urlEdit . '" class="btn btn-success" style="margin-right: 10px">Edit</a>';
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('admin.course.index');
    }

    public function article(Request $request) {
        $data = Course::with('category')->where('is_article', true)->get();
        if ($request->ajax()) {
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('title', function($row) {
                    return $row->title;
                })
                ->addColumn('category', function($row) {
                    return $row->category->name;
                })
                ->addColumn('action', function($row) {
                    $urlEdit = route('course.edit.article', $row->id);
                    return '<a href="' . $urlEdit . '" class="btn btn-success" style="margin-right: 10px">Edit</a>';
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('admin.article.index');
    }

    public function createArticle() {
        return view('admin.article.create_article', [
            'categories' => Category::all(),
        ]);
    }

    public function editArticle(Course $course) {
        return view('admin.article.edit_article', [
            'categories' => Category::all(),
            'article' => Article::where('course_id', $course->id)->first(),
            'course' => Course::where('id', $course->id)->first()
        ]);
    }

    public function createChapter() {
        return view('admin.course.create_chapter', [
            'categories' => Category::all(),
        ]);
    }

    public function editChapter(Course $course) {
        return view('admin.course.edit_chapter', [
            'categories' => Category::all(),
            'course' => Course::with('chapter')->where('id', $course->id)->first()
        ]);
    }

    public function storeArticle(Request $request) {
        $image = $request->file('image');
        $ext = $image->getClientOriginalExtension();
        $imageName = Carbon::now()->format('dmYHis') . '.' . $ext;

        $image->move('storage/image/', $imageName);
        $request->imagePath = 'storage/image/' . $imageName;

        $premium = false;
        if($request->has('is_premium')){
            $premium = true;
        }

        $course = Course::create([
            'title' => $request->name,
            'description' => $request->description,
            'category_id' => $request->category_id,
            'is_article' => true,
            'banner' => $request->imagePath
        ]);

        Article::create([
            'course_id' => $course->id,
            'content' => $request->article_content,
            'title' => $request->article_title,
            'is_premium' => $premium
        ]);

        return redirect(route('course.article.index'))->with('success', 'Successfully add a new article');
    }

    public function updateArticle(Course $course, Request $request) {
        $image = $request->file('image');
        if(!is_null($image)){
            $ext = $image->getClientOriginalExtension();
            $imageName = Carbon::now()->format('dmYHis') . '.' . $ext;

            $image->move('storage/image/', $imageName);
            $request->imagePath = 'storage/image/' . $imageName;
        }else {
            $request->imagePath = $course->banner;
        }

        $premium = false;
        if($request->has('is_premium')){
            $premium = true;
        }

        Course::where('id', $course->id)->update([
            'title' => $request->name,
            'description' => $request->description,
            'category_id' => $request->category_id,
            'is_article' => true,
            'banner' => $request->imagePath
        ]);

        Article::where('course_id', $course->id)->update([
            'course_id' => $course->id,
            'content' => $request->article_content,
            'title' => $request->article_title,
            'is_premium' => $premium
        ]);

        return redirect(route('course.article.index'))->with('success', 'Successfully edit an article');
    }

    public function storeChapter(Request $request) {
        $image = $request->file('image');
        $ext = $image->getClientOriginalExtension();
        $imageName = Carbon::now()->format('dmYHis') . '.' . $ext;

        $image->move('storage/image/', $imageName);
        $request->imagePath = 'storage/image/' . $imageName;

        $course = Course::create([
            'title' => $request->name,
            'description' => $request->description,
            'category_id' => $request->category_id,
            'is_article' => false,
            'banner' => $request->imagePath
        ]);

        foreach($request->title as $key => $title) {
            $isPremium = false;
            if($request->has('premium')){
                foreach($request->premium as $premium) {
                    if ($key == $premium) {
                        $isPremium = true;
                    }
                }
            }
            Chapter::create([
                'course_id' => $course->id,
                'title' => $title,
                'url'=> $request->url[$key],
                'is_premium' => $isPremium
            ]);
        }

        return redirect(route('course.chapter.index'))->with('success', 'Successfully add a new chapter');
    }

    public function updateChapter(Course $course, Request $request) {
        $image = $request->file('image');
        if(!is_null($image)){
            $ext = $image->getClientOriginalExtension();
            $imageName = Carbon::now()->format('dmYHis') . '.' . $ext;

            $image->move('storage/image/', $imageName);
            $request->imagePath = 'storage/image/' . $imageName;
        }else {
            $request->imagePath = $course->banner;
        }

        Course::where('id', $course->id)->update([
            'title' => $request->name,
            'description' => $request->description,
            'category_id' => $request->category_id,
            'is_article' => false,
            'banner' => $request->imagePath
        ]);

        Chapter::where('course_id', $course->id)->delete();

        foreach($request->title as $key => $title) {
            $isPremium = false;
            if($request->has('premium')){
                foreach($request->premium as $premium) {
                    if ($key == $premium) {
                        $isPremium = true;
                    }
                }
            }
            Chapter::create([
                'course_id' => $course->id,
                'title' => $title,
                'url'=> $request->url[$key],
                'is_premium' => $isPremium
            ]);
        }

        return redirect(route('course.chapter.index'))->with('success', 'Successfully edit a chapter');
    }
}
