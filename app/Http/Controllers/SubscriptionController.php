<?php

namespace App\Http\Controllers;

use App\Models\Subscription;
use Illuminate\Http\Request;

class SubscriptionController extends Controller
{
    public function index()
    {
        return view('admin.subscription.index', [
            'subscriptions' => Subscription::paginate(5)
        ]);
    }

    public function create()
    {
        return view('admin.subscription.create');
    }

    public function store(Request $request)
    {
        Subscription::create([
            'name' => $request->name,
            'type' => $request->type,
            'price' => $request->price,
            'price_percentage' => $request->price_percentage,
            'status' => Subscription::STATUS_ACTIVE
        ]);

        return redirect(route('subscription.index'))->with('success', 'successfully add new subscription type');
    }

    public function edit(Subscription $subscription)
    {
        return view('admin.subscription.edit', [
            'subscription' => $subscription
        ]);
    }

    public function update(Request $request, Subscription $subscription)
    {
        $subscription->update([
            'name' => $request->name,
            'type' => $request->type,
            'price' => $request->price,
            'price_percentage' => $request->price_percentage
        ]);
        return redirect(route('subscription.index'))->with('success', 'subscription updated');
    }

    public function changeStatus(Subscription $subscription)
    {
        $subscription->update([
            'status' => Subscription::STATUS_INACTIVE
        ]);

        return redirect(route('payment_method.index'))->with('success', 'subscription status changed');
    }
}
