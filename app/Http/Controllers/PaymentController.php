<?php

namespace App\Http\Controllers;

use App\Models\Invoice;
use App\Models\PaymentMethod;
use App\Models\Subscription;
use App\Models\UserSubscription;
use App\Services\InvoiceService;
use App\Services\SubscriptionService;
use Carbon\Carbon;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    private $invoiceService;
    private $subscriptionService;

    public function __construct(InvoiceService $invoiceService, SubscriptionService $subscriptionService)
    {
        $this->invoiceService = $invoiceService;
        $this->subscriptionService = $subscriptionService;
    }

    public function verifyPayment(Request $request): RedirectResponse {
        $this->invoiceService->verifyPayment($request->user_id);

        return redirect()->back();
    }

    public function updateSubscription(Subscription $subscription, Request $request): RedirectResponse {
        $this->subscriptionService->updateSubscription($subscription, $request);

        return redirect()->back();
    }

    public function index(){
        return view('payment',[
            'invoice' => Invoice::with(['subscription','paymentMethod'])->where([
                'user_id' => auth()->user()->id,
                'is_paid' => Invoice::STATUS_UNPAID
            ])->first(),
            'paymentMethods' => PaymentMethod::where('status', PaymentMethod::STATUS_ACTIVE)->get(),
            'subscriptions' => Subscription::where('status', Subscription::STATUS_ACTIVE)->get(),
            'userSubscription' => UserSubscription::where([
                'user_id' => auth()->user()->id,
                'status' => UserSubscription::STATUS_ACTIVE
            ])->first()
        ]);
    }

    public function createInvoice(Request $request) {
        $latestInvoice = Invoice::latest()->first();
        if(is_null($latestInvoice)){
            $nextInvoiceNumber = 1;
        }else {
            $nextInvoiceNumber = explode(Invoice::INVOICE_PREFIX, $latestInvoice->invoice_number)[1] + 1;
        }
        $subs = Subscription::where('id', $request->subscription)->first();
        Invoice::create([
            'user_id' => auth()->user()->id,
            'subscription_id' => $request->subscription,
            'payment_method_id' => $request->payment_method,
            'price' => $subs->price - ($subs->price * $subs->price_percentage / 100),
            'is_paid' => Invoice::STATUS_UNPAID,
            'invoice_number' => Invoice::INVOICE_PREFIX . $nextInvoiceNumber
        ]);

        return redirect(route('payment.index'))->with('success', 'payment has been created');
    }

    public function uploadPaymentSlip(Request $request) {
        $invoice = Invoice::find($request->invoice_id);
        if($invoice->user_id != auth()->user()->id) {
            abort(403);
        }

        $image = $request->file('image');
        $ext = $image->getClientOriginalExtension();
        $imageName = Carbon::now()->format('dmYHis') . '.' . $ext;

        $image->move('storage/image/', $imageName);
        $request->imagePath = 'storage/image/' . $imageName;

        $invoice->update([
           'payment_slip' => $request->imagePath
        ]);

        return redirect(route('payment.index'))->with('success', 'success upload payment slip');
    }
}
