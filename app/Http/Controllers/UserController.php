<?php

namespace App\Http\Controllers;

use App\Models\Invoice;
use App\Models\User;
use App\Models\UserSubscription;
use App\Services\InvoiceService;
use App\Services\SocialService;
use App\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

class UserController extends Controller
{
    private $userService;
    private $socialService;
    private $invoiceService;

    public function __construct(UserService $userService, SocialService $socialService, InvoiceService $invoiceService)
    {
        $this->userService = $userService;
        $this->socialService = $socialService;
        $this->invoiceService = $invoiceService;
    }

    public function updateProfile(Request $request) {
        $this->userService->updateProfile();
    }

    public function updateSocials(Request $request) {
        $this->socialService->updateSocial();
    }

    public function adminDashboard() {
        return view('admin.dashboard');
    }

    public function index(Request $request) {
        $data = User::with(['userSubscription' => function($query) {
            $query->with('payment')->where('status', UserSubscription::STATUS_ACTIVE)->orderBy('id', 'desc')->first();
        }, 'invoices'])->get();

        if ($request->ajax()) {
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('subscription', function($row) {
                    return optional(optional($row->userSubscription->last())->payment)->name ?? null;
                })
                ->addColumn('invoice_number', function($row) {
                    return optional($row->invoices->last())->invoice_number ?? null;
                })
                ->addColumn('image', function($row) {
                    if(is_null(optional($row->invoices->last())->payment_slip)){
                        return "-";
                    }else {
                        $url = asset(optional($row->invoices->last())->payment_slip);
                        return '<a href="' . $url . '" data-toggle="modal" data-target="#modal-lg-' . $row->id . '">
                            <img src="' . $url . '" class="img-fluid" alt="payment slip" />
                        </a>
                        <div class="modal fade" id="modal-lg-' . $row->id . '">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title">Payment Slip</h4>
                                    </div>
                                    <div class="modal-body text-center">
                                            <img src="' . $url . '" class="img-fluid mb-2" alt="payment slip" />
                                    </div>
                                </div>
                            </div>
                        </div>';
                    }
                })
                ->addColumn('action', function($row) {
                    if(count($row->invoices) == 0){
                        return "-";
                    }else {
                        if(!optional($row->invoices->last())->is_paid){
                            $urlVerify = route('user.verify', $row->id);
                            return '<a href="' . $urlVerify . '" class="btn btn-success" style="margin-right: 10px">Verify</a>';
                        }else {
                            return "Verified";
                        }
                    }

                })
                ->rawColumns(['action', 'image'])
                ->make(true);
        }

        return view('admin.user.index');
    }

    public function verify(User $user) {
        $this->invoiceService->verifyPayment($user->id);

        return redirect(route('user.index'))->with('success', 'successfully verify user payment');
    }
}
