<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Course;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ArticleController extends Controller
{
    public function show(Article $article) {
        $userSubs = DB::table('users')
            ->join('user_subscriptions', 'user_subscriptions.user_id', '=','users.id')
            ->where('users.id', auth()->user()->id)
            ->latest('user_subscriptions.created_at')->first();
        if($article->is_premium && is_null($userSubs)) {
            if(auth()->user()->roles[0]->name != 'admin'){
                return redirect(route('payment.index'));
            }
        }else if($article->is_premium && $userSubs->expired_at <= Carbon::now()){
            if(auth()->user()->roles[0]->name != 'admin'){
                return redirect(route('payment.index'));
            }
        }

        return view('article', [
            'course' => Course::where('id', $article->course_id)->first(),
            'article' => $article,
        ]);
    }
}
