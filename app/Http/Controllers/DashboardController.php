<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Course;
use App\Services\CourseService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{

    private $courseService;

    public function __construct(CourseService $courseService)
    {
        $this->courseService = $courseService;
    }

    public function dashboard(Request $request) {
        $data = [];
        $total = 0;
        if(isset($request->type)){
            $data = $this->courseService->fetchByType($request->type);
            $total = $this->courseService->courseCountByType($request->type);
        }else {
            $data = $this->courseService->fetchAll();
            $total = $this->courseService->courseCount();
        }

        if(isset($request->category)){
            $data = Course::where('category_id', $request->category)->paginate(6);
            $total = DB::table('courses')->where('category_id', $request->category)->count();
        }

        return view('dashboard', [
            'categories' => Category::all(),
            'courses' => $data,
            'total' => $total,
            'type' => $request->type
        ]);
    }

    public function about() {
        return view('about');
    }
}
