<?php

namespace App\Http\Controllers;

use App\Models\PaymentMethod;
use Illuminate\Http\Request;

class PaymentMethodController extends Controller
{
     public function index()
    {
        return view('admin.payment_method.index', [
            'paymentMethods' => PaymentMethod::all()
        ]);
    }

    public function create()
    {
        return view('admin.payment_method.create');
    }

    public function store(Request $request)
    {
        PaymentMethod::create([
            'name' => $request->name,
            'account_number' => $request->account_number,
            'status' => PaymentMethod::STATUS_ACTIVE
        ]);

        return redirect(route('payment_method.index'))->with('success', 'successfully add new payment method');
    }

    public function edit(PaymentMethod $paymentMethod)
    {
        return view('admin.payment_method.edit', [
            'paymentMethod' => $paymentMethod
        ]);
    }

    public function update(Request $request, PaymentMethod $paymentMethod)
    {
        $paymentMethod->update([
            'name' => $request->name,
            'account_number' => $request->account_number
        ]);
        return redirect(route('payment_method.index'))->with('success', 'payment method updated');
    }

     public function changeStatus(PaymentMethod $paymentMethod)
    {
        $paymentMethod->update([
            'status' => PaymentMethod::STATUS_INACTIVE
        ]);

        return redirect(route('payment_method.index'))->with('success', 'payment method status changed');
    }
}
