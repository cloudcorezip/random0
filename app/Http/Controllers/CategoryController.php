<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Services\CategoryService;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    private $categoryService;

    public function __construct(CategoryService $categoryService)
    {
        $this->categoryService = $categoryService;
    }

    public function index() {
        return view('admin.category.index', [
            'categories' => $this->categoryService->getCategories()
        ]);
    }

    public function create() {
        return view('admin.category.create');
    }

    public function store(Request $request) {
        $this->categoryService->create($request);

        return redirect(route('category.index'))->with('success', 'Success create new category');
    }

    public function edit(Category $category) {
        return view('admin.category.edit', [
            'category' => $category
        ]);
    }

    public function update(Category $category, Request $request) {
        $this->categoryService->update($category, $request);

        return redirect(route('category.index'))->with('success', 'Success update category');
    }

    public function changeStatus(Category $category): RedirectResponse
    {
        $this->categoryService->changeStatus($category);

        return redirect()->back()->with('success', 'Success change category status');
    }
}
