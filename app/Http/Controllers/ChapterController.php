<?php

namespace App\Http\Controllers;

use App\Models\Chapter;
use App\Models\Course;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ChapterController extends Controller
{
    public function show(Chapter $chapter) {
        $userSubs = DB::table('users')
            ->join('user_subscriptions', 'user_subscriptions.user_id', '=','users.id')
            ->where('users.id', auth()->user()->id)
            ->latest('user_subscriptions.created_at')->first();
        if($chapter->is_premium && is_null($userSubs)) {
            if(auth()->user()->roles[0]->name != 'admin'){
                return redirect(route('payment.index'));
            }
        }else if($chapter->is_premium && $userSubs->expired_at <= Carbon::now()){
            if(auth()->user()->roles[0]->name != 'admin'){
                return redirect(route('payment.index'));
            }
        }

        return view('chapter', [
            'course' => Course::where('id', $chapter->course_id)->first(),
            'chapter' => $chapter,
            'next_course' => $chapter->where('course_id', $chapter->course_id)
                            ->where('id', '>', $chapter->id)
                            ->first()
        ]);
    }
}
