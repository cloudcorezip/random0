<?php

namespace App\Http\Middleware;

use Carbon\Carbon;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SubscriptionCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $userSubscription = DB::table('users')
            ->join('user_subscriptions', 'user_subscriptions.user_id', '=','users.id')
            ->latest()->first();

        if($userSubscription->expired_at >= Carbon::now()){
            return $next($request);
        }
        abort(403);
    }
}
