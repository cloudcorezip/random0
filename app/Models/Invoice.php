<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    use HasFactory;

    const STATUS_UNPAID = 0;
    const STATUS_PAID = 1;
    const INVOICE_PREFIX = 'INV/SUBS/';

    protected $guarded = [];

    public function subscription() {
        return $this->belongsTo(Subscription::class , 'subscription_id', 'id');
    }

    public function paymentMethod() {
        return $this->belongsTo(PaymentMethod::class, 'payment_method_id', 'id');
    }
}
