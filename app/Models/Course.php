<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function category() {
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }

    public function chapter() {
        return $this->hasMany(Chapter::class, 'course_id', 'id');
    }

    public function article() {
        return $this->hasOne(Article::class, 'course_id', 'id');
    }
}
