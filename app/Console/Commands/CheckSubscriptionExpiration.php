<?php

namespace App\Console\Commands;

use App\Models\UserSubscription;
use Carbon\Carbon;
use Illuminate\Console\Command;

class CheckSubscriptionExpiration extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:subscription';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Expiry user subscription';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $userSubscriptions = UserSubscription::all();
        foreach ($userSubscriptions as $userSubscription) {
            if (Carbon::parse($userSubscription->expired_at) < Carbon::now()){
                $userSubscription->update([
                    'status' => UserSubscription::STATUS_INACTIVE
                ]);
            }
        }
        return 0;
    }
}
