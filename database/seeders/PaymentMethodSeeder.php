<?php

namespace Database\Seeders;

use App\Models\PaymentMethod;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PaymentMethodSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('payment_methods')->insert([[
            'name' => 'OVO',
            'account_number' => '3838381828182',
            'status' => 1
        ],[
            'name' => 'GOPAY',
            'account_number' => '1123828247446464',
            'status' => 1
        ],[
            'name' => 'DANA',
            'account_number' => '4664646464646',
            'status' => 1
        ]]);
    }
}
