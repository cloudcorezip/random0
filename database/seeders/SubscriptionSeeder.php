<?php

namespace Database\Seeders;

use App\Models\Subscription;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SubscriptionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('subscriptions')->insert([[
            'name' => 'Daily',
            'type' => 'daily',
            'banner' => '',
            'price' => 50000,
            'price_percentage' => 0,
            'status' => 1
        ],[
            'name' => 'Monthly',
            'type' => 'monthly',
            'banner' => '',
            'price' => 300000,
            'price_percentage' => 0,
            'status' => 1
        ],[
            'name' => 'Annually',
            'type' => 'annually',
            'banner' => '',
            'price' => 1000000,
            'price_percentage' => 0,
            'status' => 1
        ]]);
    }
}
